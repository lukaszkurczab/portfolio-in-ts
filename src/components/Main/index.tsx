import React from 'react'
import LandingSection from '../LandingSection'
import AboutMe from '../AboutMe'
import Skills from '../Skills'
import ToolsSection from '../ToolsSection'
import MyWorks from '../MyWorks'
import PostsSection from '../PostsSection'

interface MainInterface {
  refAboutMe: string;
  refSkills: string;
  refPortfolio: string;
  refBlog: string;
}

const Main = ({ refAboutMe, refSkills, refPortfolio, refBlog }: MainInterface) => {
  return (
    <>
      <main className="mainPart" id="AboutMe">
        <LandingSection />
        <AboutMe {...{ refAboutMe }} />
        <Skills {...{ refSkills }} />
      </main>
      <ToolsSection />
      <main className="mainPart">
        <MyWorks {...{ refPortfolio }} />
        <PostsSection {...{ refBlog }} />
      </main>
    </>
  )
}

export default Main