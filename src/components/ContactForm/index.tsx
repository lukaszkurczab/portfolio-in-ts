import React from 'react'
import Button from '../Button'

const ContactForm = () => {
  return (
    <div className="emailForm_wrapper">
      <div className="inputs_wrapper">
        <form action="mailto:lukasz.kurczab@gmail.com" method="post" encType="text/plain">
          <input type="email" name="mail" placeholder="Your e-mail" className="input" /><br />
          <input type="text" name="name" placeholder="Your name" className="input" /><br />
          <textarea name="content" rows={6}
            placeholder='How can I help you? Please, put here your message/request.'
            className="input textarea"></textarea><br />
        </form>
      </div>
      <Button text="Send" />
    </div>
  )
}

export default ContactForm