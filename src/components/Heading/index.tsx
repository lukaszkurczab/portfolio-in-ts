import React from 'react'

interface HeadingInterface {
  main: string;
  secondary?: string;
}

const Heading = ({ main, secondary }: HeadingInterface) => {
  return (
    <>
      <h2 className="mainHeading">{main}</h2>
      <h3 className="secondaryHeading">{secondary}</h3>
    </>
  )
}

export default Heading