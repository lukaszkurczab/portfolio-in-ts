import React from 'react'

const scrollToRef = (ref: any) => window.scrollTo(0, ref.current.offsetTop)

interface InnerNavigationItemInterface {
  nav: string;
  text: string;
}

const InnerNavigationItem = ({ nav, text }: InnerNavigationItemInterface) => {
  return (
    <>
      <li className="nav_item"><span onClick={() => scrollToRef(nav)} className="navLink">{text}</span></li>
    </>
  )
}

export default InnerNavigationItem