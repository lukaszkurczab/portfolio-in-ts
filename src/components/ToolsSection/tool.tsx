import React from 'react'

interface ToolInterface {
  name: string;
  version: string;
  src: string;
}

const Tool = ({ name, version, src }: ToolInterface) => {
  return (
    <div className="tool_wrapper">
      <div className="toolBox">
        <img src={src} alt={name} className="toolLogo" />
        <p className="text">{name}</p>
        <p className="text">{version}</p>
      </div>
    </div>
  )
}

export default Tool