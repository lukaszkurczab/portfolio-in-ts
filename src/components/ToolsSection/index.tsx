import React from 'react'
import Heading from '../Heading'
import Tool from './tool'

import ReactImg from '../../assets/react.svg'
import WebpackImg from '../../assets/webpack.svg'
import ExpressImg from '../../assets/express.svg'
import StyledComponentsImg from '../../assets/styledComponents.png'
import ReduxImg from '../../assets/redux.svg'
import FlexboxImg from '../../assets/flexbox.svg'
import ToolsImg from '../../assets/tools.png'
import BgImg from '../../assets/koła_01.png'

const ToolsSection = () => {

  const toolsList = [
    { 'name': "React", 'version': "16.6.3", 'src': ReactImg },
    { 'name': "Webpack", 'version': "4.19.1", 'src': WebpackImg },
    { 'name': "Express", 'version': "4.16.4", 'src': ExpressImg },
    { 'name': "Styled Components", 'version': "4.16.4", 'src': StyledComponentsImg },
    { 'name': "Redux", 'version': "4.0.1", 'src': ReduxImg },
    { 'name': "Flexbox", 'version': "4.0.1", 'src': FlexboxImg },
    { 'name': "Program", 'version': "5.2.1", 'src': ToolsImg }
  ]

  return (
    <div className="tools_wrapper">
      <div className="toolsText_wrapper">
        <Heading main="Tools" secondary="My essentials" />
      </div>
      
      <div className="toolsList_wrapper">
        {
          toolsList.map(program => {
            return <Tool key={program.name} name={program.name} version={program.version} src={program.src} />
          })
        }
      </div>

      <img src={BgImg} alt="" className="bgImg" />
    </div>
  )
}

export default ToolsSection