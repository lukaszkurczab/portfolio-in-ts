import React, { useEffect, useState } from 'react'
import Button from '../Button'
import LinkBox from '../LinkBox'

import BgImg1 from '../../assets/koła_03.png'
import BgImg2 from '../../assets/koła_01.png'

const LandingSection = () => {
  const [show, setShow] = useState(false)

  useEffect(() => {
    const timeout = setTimeout(() => {
      setShow(true)
    }, 100);
    return () => clearTimeout(timeout);
  }, []);

  return (
    <div className="hello_wrapper">
      <div className="photo_wrapper">
        <img src={BgImg1} alt="" className="aboutMe__top-leftCircle" />
      </div>

      <div className={show ? "introduce_wrapper outOfLayoutBox" : "introduce_wrapper outOfLayoutBox notViewed"}>
        <div className="text_wrapper">
          <h1 className="mainHeading">Hi, My name is Łukasz Kurczab</h1>
          <h2 className="secondaryHeading">Front-end developer</h2>
          <p className="text">Passionate Techy and Tech Author with 2 years of experience within the field.</p>
        </div>

        <div className="repo_wrapper">
          <p className="text">See my works</p>
          <LinkBox gitLink="http://github.com/lukaszkurczab" projectLink="#portfolio" />
        </div>
      </div>

      <div className="files_wrapper">
        <h2 className="mainHeading">I am a freelancer</h2>
        <p className="text">Contack me if you want to work with me</p>
        <div className="buttons_wrapper">
          <form action="#contactMe">
            <Button text="Hire me!" />
          </form>
          <form target="_blank" action="./data/CV Łukasz Kurczab.pdf">
            <Button text="Download CV" />
          </form>
          <img src={BgImg2} alt="" className="aboutMe__bottom-rightCircle" />
        </div>
      </div>
    </div>
  )
}

export default LandingSection