import React from 'react'
import Heading from '../Heading'

import BgImg from '../../assets/koła_02.png'
import CourseLogo from '../../assets/easyCode.png'

interface AboutMeInterface {
  refAboutMe: string;
}

const AboutMe = ({ refAboutMe }: AboutMeInterface) => {
  return (
    <div className="aboutMe_wrapper" ref={refAboutMe}>
      <img src={BgImg} alt="" className="aboutMe__bottom-leftCircle" />
      <div className="text_wrapper">
        <Heading main="About Me" secondary="All about Techy" />
        <p className="text">My first contact with programming was in high school, where I tried to learn Python out of curiosity, but I quickly got discouraged. I returned to code at the end of my studies (2019), when I started learning the basics of HTML. Thanks to the ability to create not only scripts, but also layouts and the enormous freedom of creation, I got involved and started to learn more and more about web technologies. I was mainly focused on the front-end, but I also had a stage when I was learning php.</p>
        <h3 className="secondaryHeading">My interests</h3>
        <ul className="interestsList">
          <li>music</li>
          <li>mountain trips</li>
          <li>board games</li>
        </ul>
      </div>

      <div className="course_wrapper">
        <p className="text">I learned most of my skills on the Udemy</p>
        <a href="http://easy-code.io/" target="_blank" rel="noreferrer">
          <img src={CourseLogo} alt="Easy Code logo" className="logo" />
        </a>
      </div>
    </div>
  )
}

export default AboutMe