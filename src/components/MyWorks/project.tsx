import React from 'react'
import LinkBox from '../LinkBox'

interface ProjectInterface {
  src: string;
  alt: string;
  heading: string;
  type: string;
  tech: string;
  gitLink: string;
  projectLink: string;
}

const Project = ({ src, alt, heading, type, tech, gitLink, projectLink }: ProjectInterface) => {
  return (
    <div className="project_wrapper">
      <div className="projectBox">
        <img src={src} alt={alt} className="projectImg" />
        <LinkBox gitLink={gitLink} projectLink={projectLink} />
        <div className="descBox">
          <h3 className="projHeading">{heading}</h3>
          <p className="projDesc">- {type}</p>
          <p className="projDesc">{tech}</p>
        </div>
      </div>
    </div>
  )
}

export default Project