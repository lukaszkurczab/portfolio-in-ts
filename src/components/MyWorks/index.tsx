import React from 'react'
import Heading from '../Heading'
import Project from './project'

import FyloImg from '../../assets/portfolio/fylo.png'
import ManagaImg from '../../assets/portfolio/manage.png'
import JobsImg from '../../assets/portfolio/jobs.png'
import SocialMediaImg from '../../assets/portfolio/socialMedia.png'
import SoundstripeImg from '../../assets/portfolio/soundstripe.png'
import TraackrImg from '../../assets/portfolio/traackr.png'

interface MyWorksInterface {
  refPortfolio: string
}

const MyWorks = ({ refPortfolio }: MyWorksInterface) => {

  const projectsList = [
    {
      'src': FyloImg,
      'name': "Fylo",
      'heading': "Office software",
      'type': "data storage",
      'tech': "Flexbox / Scss / RWD",
      'gitLink': "https://github.com/lukaszkurczab/fylo-dark-theme-landing-page-master",
      'projectLink': "#"
    },
    {
      'src': ManagaImg,
      'name': "Manage",
      'heading': "Office software",
      'type': "data analytics",
      'tech': "Flexbox / Scss / RWD",
      'gitLink': "https://github.com/lukaszkurczab/manage-landing-page-master",
      'projectLink': "#"
    },
    {
      'src': JobsImg,
      'name': "Jobs",
      'heading': "E-commerce",
      'type': "job search",
      'tech': "Scss / JS / React",
      'gitLink': "https://github.com/lukaszkurczab/static-job-listings-master",
      'projectLink': "#"
    },
    {
      'src': SocialMediaImg,
      'name': "Social Media",
      'heading': "E-commerce",
      'type': "data analytics",
      'tech': "Flexbox / React / RWD",
      'gitLink': "https://github.com/lukaszkurczab/social-media-dashboard-with-theme-switcher-master",
      'projectLink': "#"
    },
    {
      'src': SoundstripeImg,
      'name': "Soundstripe",
      'heading': "E-commerce",
      'type': "online shop",
      'tech': "Flexbox / React / RWD",
      'gitLink': "#",
      'projectLink': "#",
    },
    {
      'src': TraackrImg,
      'name': "Traackr",
      'heading': "Office software",
      'type': "data storage",
      'tech': "Flexbox / Scss / React",
      'gitLink': "#",
      'projectLink': "#",
    }
  ]

  return (
    <div className="myWorks_wrapper" id="portfolio" ref={refPortfolio}>
      <div className="myWorks_text_wrapper">
        <Heading main="My works" secondary="Portfolio" />
        <p className="text">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Tempore dolore distinctio nulla
          aliquid in cumque ducimus odit, labore harum minus cupiditate culpa temporibus possimus nisi quod sequi recusandae tempora accusantium. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nesciunt aliquid tenetur  pariatur quas, repellendus facilis fugit ad recusandae quidem itaque natus amet quaerat ullam obcaecati magnam omnis nobis minima vero?</p>
      </div>

      <div className="projects_wrapper">
        {
          projectsList.map(project => {
            return (
              <Project
                key={project.name}
                src={project.src}
                alt={project.name}
                heading={project.heading}
                type={project.type}
                tech={project.tech}
                gitLink={project.gitLink}
                projectLink={project.projectLink}
              />)
          })
        }
      </div>
    </div>
  )
}

export default MyWorks