import React from 'react'

interface ButtonInterface {
  text: string;
}

const Button = ({ text }: ButtonInterface) => {
  return (
    <>
      <button type="submit" className="buttonBig">{text}</button>
    </>
  )
}

export default Button