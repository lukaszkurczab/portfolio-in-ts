import React from 'react'

import Next from '../../assets/blog/goNext.svg'

interface PostInterface {
  src: string;
  alt: string;
  heading: string;
  secondary: string;
  author: string;
  date: string;
  text: string;
}

const Post = ({ src, alt, heading, secondary, author, date, text }: PostInterface) => {
  return (
    <div className="post_wrapper">
      <div className="img_wrapper">
        <img src={src} alt={alt} className="postImg" />
      </div>
      <div className="text_wrapper">
        <div className="heading_wrapper">
          <h4 className="mainHeading">{heading}</h4>
          <h5 className="secondaryHeading">{secondary}</h5>
        </div>
        <div className="data_wrapper">
          <p className="dataText">{author}, {date}</p>
        </div>

        <div className="desc_wrapper">
          <p className="text">{text}</p>
          <button className="readMore">
            Read more <img src={Next} alt="Next" className="readMore_img" />
          </button>
        </div>
      </div>
    </div>
  )
}

export default Post