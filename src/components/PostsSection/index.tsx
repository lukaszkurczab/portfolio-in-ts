import React from 'react'
import Heading from '../Heading'
import Post from './post'

import BgImg from '../../assets/koła_01.png'
import PostOneImg from '../../assets/blog/title01.jpg'
import PostTwoImg from '../../assets/blog/title02.jpg'

interface PostsSectionInterface {
  refBlog: string;
}

const PostsSection = ({ refBlog }: PostsSectionInterface) => {

  const postsList = [
    {
      "src": PostOneImg,
      "name": "Post 1",
      "heading": "Title 01",
      "secondary": "Secondary Title",
      "author": "author",
      "date": "01.09.2020",
      "text": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque laboriosam doloremque facilis repudiandae accusamus distinctio blanditiis adipisci, beatae consequatur dicta sunt, ipsa neque amet, cumque odio! Deleniti magni explicabo soluta!"
    },
    {
      "src": PostTwoImg,
      "name": "Post 2",
      "heading": "Title 02",
      "secondary": "Secondary Title",
      "author": "author",
      "date": "01.09.2020",
      "text": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque laboriosam doloremque facilis repudiandae accusamus distinctio blanditiis adipisci, beatae consequatur dicta sunt, ipsa neque amet, cumque odio! Deleniti magni explicabo soluta!"
    }
  ]

  return (
    <div className="blog_wrapper" id="blog" ref={refBlog}>
      <Heading main="Blog posts" secondary="Hints and tips" />

      {
        postsList.map(post => {
          return (
            <Post
              key={post.name}
              src={post.src}
              alt={post.name}
              heading={post.heading}
              secondary={post.secondary}
              author={post.author}
              date={post.date}
              text={post.text}
            />
          )
        })
      }

      <img src={BgImg} alt="" className="bgImg" />
    </div>
  )
}

export default PostsSection