import React from 'react'

interface ExternalNavigationItemInterface {
  link: string;
  src: string;
  alt: string;
}

const ExternalNavigationItem = ({ link, src, alt }: ExternalNavigationItemInterface) => {
  return (
    <>
      <li className="nav_item">
        <a href={link} target="_blank" rel="noreferrer">
          <img src={src} alt={alt} className="nav_logo" />
        </a>
      </li>
    </>
  )
}

export default ExternalNavigationItem