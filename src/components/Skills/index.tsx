import React from 'react'
import Heading from '../Heading'
import Skill from './skill'

import BgImg from '../../assets/koła_03.png'

interface SkillsInterface {
  refSkills: string;
}

const Skills = ({ refSkills }: SkillsInterface) => {

  const skillsList = [
    { 'name': "HTML", 'percent': "80%" },
    { 'name': "JS", 'percent': "60%" },
    { 'name': "CSS", 'percent': "80%" },
    { 'name': "React", 'percent': "50%" },
    { 'name': "TypeScript", 'percent': "40%" },
    { 'name': "GIT", 'percent': "70%" }
  ]

  return (
    <div className="skills_wrapper" id="skills" ref={refSkills}>
      <Heading main="Skills" />
      <p className="text">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Expedita illo impedit earum atque?</p>
      <div className="graph_wrapper">
        {
          skillsList.map(skill => {
            return <Skill key={skill.name} skill={skill.name} percent={skill.percent} />
          })
        }
      </div>

      <img src={BgImg} alt="" className="skills_circle" />
    </div>
  )
}

export default Skills