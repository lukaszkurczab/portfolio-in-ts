import React from 'react'
import Heading from '../Heading'
import ContactForm from '../ContactForm'

import BgImg from '../../assets/koła_03.png'
import ContactImg from '../../assets/kontakt.png'
import PhotoImg from '../../assets/man_footer.jpeg'

interface FooterInterace {
  refContact: string;
}

const Footer = ({ refContact }: FooterInterace) => {
  return (
    <footer className="footer" id="contactMe" ref={refContact}>
      <div className="sider">
        <Heading main="Contact me" />
        <p className="text">If you are willing to work with me, contact me. I can join your conference to serve you with my
          engenieering experience.</p>
        <ContactForm />
      </div>

      <div className="sider">
        <img src={BgImg} alt="" className="bgCircle" />

        <div className="contactText_wrapper outOfLayoutBox">
          <div className="contactIcon_wrapper">
            <img src={ContactImg} alt="Contact" className="contactIcon" />
          </div>
          <div className="contactText">
            <p className="text">lukasz.kurczab@gmail.com</p>
            <p className="text">+48 793 724 184</p>
          </div>
        </div>

        <div className="img_wrapper">
          <img src={PhotoImg} alt="Łukasz Kurcab - contact me!" className="img" />
        </div>
        <div className="text_wrapper">
          <p className="text">autor: Łukasz Kurczab</p>
          <p className="text">username: @lkurczab</p>
          <p className="text">homepage: <a href="lkurczab.pl" className="contact_link">lkurczab.pl</a> </p>
          <p className="text">repository type: Open-source</p>
          <p className="text">url: <a href="lkurczab.pl" className="contact_link">github.com/lukaszkurczab</a></p>
        </div>
      </div>
    </footer>
  )
}

export default Footer