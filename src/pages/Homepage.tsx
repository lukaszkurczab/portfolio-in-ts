import React, { useRef } from 'react'
import Header from '../components/Header'
import Main from '../components/Main'
import Footer from '../components/Footer'

const Homepage = () => {
  const refAboutMe: any = useRef(null);
  const refSkills: any = useRef(null);
  const refPortfolio: any = useRef(null);
  const refBlog: any = useRef(null);
  const refContact: any = useRef(null);

  return (
    <>
      <Header {...{ refAboutMe, refSkills, refPortfolio, refBlog, refContact }} />
      <Main {...{ refAboutMe, refSkills, refPortfolio, refBlog }} />
      <Footer {...{ refContact }} />
    </>
  )
}

export default Homepage